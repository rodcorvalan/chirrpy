jQuery(document).ready(function($) {
	
	jQuery('.screens a').click(function(event) {
		jQuery('.screens li').removeClass('active');
		jQuery(this).parent('li').addClass('active');
		var editor = jQuery(this).data('editor');
		if ( editor == 1 )
		{
			jQuery('.panel').hide();
		}
		else
		{
			jQuery('.panel').show();
		}
		return false;
	});

});