jQuery(document).ready(function($) {
	
	// Date

	jQuery( ".input_date input" ).datepicker({
		minDate: new Date(),
	});

	jQuery(".input_datepicker").datepicker();

	/*
	// Para rangos

	var start_date = jQuery( "#start_date" ).datepicker({
		minDate: new Date(),
	});
	start_date.on( "change", function() {
		end_date.datepicker( "option", "minDate", start_date.datepicker("getDate") );
	});
	
	var end_date = jQuery( "#end_date" ).datepicker({
		minDate: new Date(),
	});
	end_date.on( "change", function() {
		start_date.datepicker( "option", "maxDate", start_date.datepicker("getDate") );
	});
	*/
	
	// Time

	jQuery('.input_time input').wickedpicker({
		now : '00:00'
	}).val('');

	// Editor

	tinymce.init({
		selector: '.textarea_editor textarea',
		toolbar: 'bold italic | alignleft aligncenter alignright alignjustify',
		menubar:false,
		statusbar: false,
	});

	// Dropzone
	/*
	Dropzone.autoDiscover = false;
	jQuery("#upload_image").dropzone({
		url : 'upload.php',
		maxFiles: 1
	});
	*/

	// Range

	var auto_darken_value = jQuery('#auto_darken_value');

	jQuery('#auto_darken').change(function()
	{
		auto_darken_value.val(this.value);
	});

	jQuery('#auto_darken').change();

	// Select2

	jQuery('select').select2({
		minimumResultsForSearch: -1,
		placeholder: function () {
			$(this).data('placeholder');
		}
	});

	// Map

	/*

	var lat = parseFloat(jQuery('#lat').val());
	var lng = parseFloat(jQuery('#lng').val());
	var lat_lng = {lat:lat,lng:lng}

	var map = new google.maps.Map(document.getElementById('map'), {
		center: lat_lng,
		zoom: 15
	});

	var marker = new google.maps.Marker({
		position: lat_lng,
		map: map,
		draggable: true
	});

	google.maps.event.addListener(marker, 'dragend', function(marker) 
	{
		var lat_lng = marker.latLng; 
        current_latitude = lat_lng.lat();
        current_longitude = lat_lng.lng();
        jQuery("#lat").val(current_latitude);
        jQuery("#lng").val(current_longitude);
	});

	*/

	// Upload Single

	jQuery(".upload_single").upload({
		//action: "//example.com/handle-upload.php",
		theme: ""
	});

	jQuery(".upload_single").on('queued',function(a,b){
		jQuery.each(b, function(index, val) {
			jQuery('.upload_single .fs-upload-target').html(val.name); 
		});
	});


	// Upload Multiple

	jQuery(".upload_multiple").upload({
		//action: "//example.com/handle-upload.php",
		theme: ""
	});

	jQuery(".upload_multiple").on('queued',function(a,b){
		jQuery.each(b, function(index, val) {
			jQuery('.upload_queue .list').append('<p>'+val.name+'</p>'); 
		});
	});

});