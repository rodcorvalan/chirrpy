jQuery(document).ready(function($) {
	jQuery('.event_lightbox_close').click(function(event) {
		jQuery('.event_lightbox').fadeOut();
	});
	jQuery('.event_lightbox_open').click(function(event) {
		jQuery('.event_lightbox').fadeIn();
	});
});