jQuery(document).ready(function($) {

	jQuery('.accordion.close .content').hide();

	jQuery('.accordion a.title').click(function(event) {
		if ( jQuery(this).parent('.accordion').hasClass('close') )
		{
			jQuery(this).removeClass('close');
			jQuery(this).parent('.accordion').removeClass('close');
			jQuery(this).parent('.accordion').children('.content').slideDown();
		}
		else
		{
			jQuery(this).addClass('close');
			jQuery(this).parent('.accordion').addClass('close');
			jQuery(this).parent('.accordion').children('.content').slideUp();
		}
		return false;
	});
});