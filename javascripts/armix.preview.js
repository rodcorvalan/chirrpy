jQuery(document).ready(function($) {
	
	// Map

	var lat = parseFloat(jQuery('#map_1').data('lat'));
	var lng = parseFloat(jQuery('#map_1').data('lng'));
	var lat_lng = {lat:lat,lng:lng}

	var map = new google.maps.Map(document.getElementById('map_1'), {
		center: lat_lng,
		zoom: 14
	});

	var marker = new google.maps.Marker({
		position: lat_lng,
		map: map,
	});

	// Map

	var lat = parseFloat(jQuery('#map_2').data('lat'));
	var lng = parseFloat(jQuery('#map_2').data('lng'));
	var lat_lng = {lat:lat,lng:lng}

	var map = new google.maps.Map(document.getElementById('map_2'), {
		center: lat_lng,
		zoom: 14
	});

	var marker = new google.maps.Marker({
		position: lat_lng,
		map: map,
	});

});