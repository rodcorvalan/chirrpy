jQuery(document).ready(function($) {
	jQuery('.tabs ul li.tab a').click(function(event) {
		var current_tab = jQuery(this).data('tab');
		jQuery('.tabs ul li.tab').removeClass('active');
		jQuery(this).parent('li.tab').addClass('active');
		jQuery('.tabs_content .tab_content').hide();
		jQuery('.tabs_content .tab_content_'+current_tab).show();

		if ( current_tab == 2 )
		{
			jQuery('.event_lightbox').fadeIn();
		}
		else
		{
			jQuery('.event_lightbox').fadeOut();
		} 

		return false;
	});
});