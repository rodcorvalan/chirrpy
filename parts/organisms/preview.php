<div class="preview event_view">

	<div class="event_view_container">

		<div class="event_cover">
			<img src="images/cover.jpg" />
			<div class="overlay_text">
				<h2 class="title">Event Title</h2>
				<p class="location">At: <span>Location</span></p>
				<p class="date">April 16th, 2017</p>
			</div>
		</div>

		<div class="event_information">

			<div class="left">
				<div class="line_1">
					<h2 class="title">EVENT TITLE</h2>
					<span class="separator">-</span>
					<p class="location">At: <span>Location</span></p>
				</div>
				<div class="line_2">
					<p class="date">FRI, AUG 26 @ 9:00 AM, Nashville, TN</p>
				</div>
				<div class="line_3">
					<p class="host">Hosted, By: <span>Organizer</span></p>
				</div>
			</div>
			<div class="right">
				<a href="#" class="button_1 event_lightbox_open">GET TICKETS</a>
			</div>
		</div>

		<div class="box_content full_width">
			<h2 class="box_content_title_1">DESCRIPTION</h2>
			<div class="text_content">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae ea fugit, dolor obcaecati consequatur nostrum illo dolore, sequi fuga eum minus? Quo aliquid magnam soluta adipisci necessitatibus debitis velit quisquam.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus nulla, nesciunt. Molestiae id inventore iure, repellendus excepturi nemo numquam deserunt sapiente aliquid, autem illum ipsa itaque dicta nostrum ipsam a.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus fuga, veritatis pariatur eos, corporis laborum corrupti rem. Ullam, quidem culpa dolorem saepe, tempore dolor doloribus natus eligendi voluptatem quos facere!
			</div>
		</div>

		<div class="box_content full_width">
			<h2 class="box_content_title_2">TICKET</h2>
			<?php include 'ticket-table.php'; ?>
		</div>

		<div class="box_content half_width">
			<h2 class="box_content_title_2">MAP</h2>
			<div class="map" id="map_1" data-lat="-32.8916448" data-lng="-68.8687189"></div>
		</div>

		<div class="box_content_clear"></div>

		<div class="box_content full_width">
			<h2 class="box_content_title_2">MAP</h2>
			<div class="map" id="map_2" data-lat="-32.8916448" data-lng="-68.8687189"></div>
		</div>

		<div class="box_content_clear"></div>


		<div class="box_content half_width">
			<h2 class="box_content_title_2">VIDEO</h2>
			<div class="video">
				<video controls>
					<source src="images/dizzy.mp4" type="video/mp4">
					Your browser does not support the video tag.
				</video> 
			</div>
		</div>

		<div class="box_content_clear"></div>

		<div class="box_content full_width">
			<h2 class="box_content_title_2">VIDEO</h2>
			<div class="video">
				<video controls>
					<source src="images/dizzy.mp4" type="video/mp4">
					Your browser does not support the video tag.
				</video> 
			</div>
		</div>

		<div class="box_content_clear"></div>

		<div class="box_content half_width">
			<h2 class="box_content_title_2">Gallery Half Page Grid</h2>
			<div class="gallery">
				<div class="image"><img src="images/cover.jpg" /></div>
				<div class="image"><img src="images/e1.jpg" /></div>
				<div class="image"><img src="images/e2.png" /></div>
				<div class="image"><img src="images/cover.jpg" /></div>
			</div>
		</div>

		<div class="box_content half_width right">
			<h2 class="box_content_title_2">Gallery Half Page Grid</h2>
			<div class="stack">
				<img src="images/cover.jpg" />
				<img src="images/e1.jpg" />
				<img src="images/e2.png" />
				<img src="images/cover.jpg" />
			</div>
		</div>

		<div class="box_content_clear"></div>

		<div class="box_content full_width">
			<h2 class="box_content_title_2">Gallery Half Page Grid</h2>
			<div class="gallery">
				<div class="image"><img src="images/cover.jpg" /></div>
				<div class="image"><img src="images/e1.jpg" /></div>
				<div class="image"><img src="images/e2.png" /></div>
				<div class="image"><img src="images/e1.jpg" /></div>
				<div class="image"><img src="images/cover.jpg" /></div>
				<div class="image"><img src="images/cover.jpg" /></div>
				<div class="image"><img src="images/cover.jpg" /></div>
				<div class="image"><img src="images/e2.png" /></div>
			</div>
		</div>

		<div class="box_content full_width">
			<h2 class="box_content_title_2">Gallery Half Page Grid</h2>
			<div class="stack">
				<img src="images/cover.jpg" />
				<img src="images/e1.jpg" />
				<img src="images/e2.png" />
				<img src="images/cover.jpg" />
				<img src="images/cover.jpg" />
				<img src="images/e1.jpg" />
				<img src="images/e2.png" />
				<img src="images/cover.jpg" />
			</div>
		</div>


	
	</div>

	




	<div class="event_lightbox">

		<div class="event_lightbox_container">
			<a class="event_lightbox_close"><span class="fa fa-close"></span></a>
			<?php include 'ticket-table.php'; ?>
		</div>
	
	</div>

</div>