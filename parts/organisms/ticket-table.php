<div class="ticket_table">
	<table>
		<thead>
			<tr>
				<th class="col_1">TICKET TYPE</th>
				<th class="col_2">PRICE</th>
				<th class="col_3">TAX & FEES</th>
				<th class="col_4">AVAIL</th>
				<th class="col_5">QTY</th>
			</tr>
		</thead>
	</table>
	<?php for ($i=0; $i < 2; $i++) : ?>
	<table class="table_body">
		<tbody>
			<tr class="row_1">
				<td class="col_1">
					<h3>GENERAL ADMISSION</h3>
					<p class="date">SUNDAY - 4-12-12</p>
				</td>
				<td class="col_2">$20.00</td>
				<td class="col_3">$2.00</td>
				<td class="col_4">50</td>
				<td class="col_5">
					<div class="input_text">
						<input type="text" name="ticket_quantity" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="5">
					<div class="ticket_description">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam fugiat facilis eaque ea deleniti delectus, quibusdam commodi hic alias est distinctio sapiente mollitia eos nihil laborum dicta, nesciunt quis velit.
					</div>
					<a href="#" class="show_full title_1">SHOW FULL DESCRIPTION<span class="fa fa-chevron-down"></span></a>
				</td>
			</tr>
		</tbody>
	</table>
	<?php endfor; ?>
	<div class="table_footer">
		<div class="row">
			<div class="total_price">TOTAL $44.79</div>
		</div>
		<div class="row">
			<div class="promo_code">
				<label for="promo_code">PROMO CODE</label>
				<input type="text" name="promo_code" id="promo_code" />
				<button class="button_2">APPLY</button>
			</div>
			<div class="buttons one_button">
				<a href="#" class="button_1">CONTINUE</a>
			</div>
		</div>
		<div class="row">
			<div class="promo_code">
				<label for="promo_code">PROMO CODE</label>
				<input type="text" name="promo_code" id="promo_code" />
				<button class="button_2">APPLY</button>
			</div>
			<div class="buttons two_buttons">
				<a href="#" class="button_1">PAY WITH PAYPAL</a>
				<a href="#" class="button_1">PAY WITH CARD</a>
			</div>
		</div>
	</div>
	<div class="policy">
		By clicking "CONTINUE", I acknowledge that I have read and agree width the Expect Events terms of service, privacy policy, and cookie policy.
	</div>
</div>