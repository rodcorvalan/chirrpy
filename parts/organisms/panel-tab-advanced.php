<div class="accordion">
	<a href="#" class="title title_1">URL<span class="fa fa-chevron-down"></span></a>
	<div class="content">
		<div class="row">
			<?php $input['placeholder'] = 'Edit URL Extension'; ?>
			<?php $input['name'] = 'edit_url_extension'; ?>
			<?php $input['required'] = false; ?>
			<?php $input['class'] = ''; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>
	</div>
</div>
<div class="accordion">
	<a href="#" class="title title_1">Reminder Email<span class="fa fa-chevron-down"></span></a>
	<div class="content">
		<div class="row">
			<?php $input['title'] = 'Send Reminder Email to Attenddes?'; ?>
			<?php $input['name'] = 'send_reminder_email'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row">
			<?php $input['placeholder'] = 'How Many Days Before'; ?>
			<?php $input['name'] = 'how_many_days_before'; ?>
			<?php $input['required'] = false; ?>
			<?php $input['class'] = ''; ?>
			<?php $input['parent_class'] = 'input_text_one_line'; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>
		<div class="row">
			<a href="#" class="link_1">
				Preview Reminder Email
			</a>
		</div>
	</div>
</div>
<div class="accordion">
	<a href="#" class="title title_1">Taxes<span class="fa fa-chevron-down"></span></a>
	<div class="content">
		<div class="title_2">
			<h3>Charges Taxes On</h3>
		</div>
		<div class="separator"></div>
		<div class="row">
			<?php $input['title'] = 'General Admission'; ?>
			<?php $input['name'] = 'general_admission'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row">
			<?php $input['title'] = 'VIP'; ?>
			<?php $input['name'] = 'vip'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row">
			<?php $input['placeholder'] = 'Tax Rate %'; ?>
			<?php $input['name'] = 'tax_rate'; ?>
			<?php $input['required'] = false; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>
	</div>
</div>
<div class="accordion">
	<a href="#" class="title title_1">Event Password<span class="fa fa-chevron-down"></span></a>
	<div class="content">
		<div class="row">
			<?php $input['title'] = 'Require Passowrd to View Event'; ?>
			<?php $input['name'] = 'require_password_to_view_event'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row">
			<?php $input['placeholder'] = 'Password'; ?>
			<?php $input['name'] = 'password_event'; ?>
			<?php $input['required'] = false; ?>
			<?php include 'parts/atoms/input-password.php'; ?>
		</div>
	</div>
</div>
<div class="accordion">
	<a href="#" class="title title_1">Price Breaks<span class="fa fa-chevron-down"></span></a>
	<div class="content">
		<div class="title_2">
			<h3>Price Break 1<span class="fa fa-close"></span></h3>
		</div>
		<div class="separator"></div>
		<div class="row">
			<div class="column_2">
				<div class="input_select">
					<label for="price_break_type">Price Break Type</label>
					<select name="price_break_type" id="price_break_type">
						<option value="0">Percentage</option>
						<option value="1">Dollar Amount</option>
						<option value="1">Free Tickets</option>
					</select>
				</div>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'Amount'; ?>
				<?php $input['name'] = 'price_break_type_amount'; ?>
				<?php include 'parts/atoms/input-text.php'; ?>
			</div>
		</div>
		<div class="row">
			<label for="free_ticket_type">Free Ticket Type</label>
			<select name="free_ticket_type" id="free_ticket_type">
				<option value="0">General Admission</option>
			</select>
		</div>
		<div class="row">
			<div class="column_2">
				<div class="input_select">
					<label for="qualifying_type">Qualifying Type</label>
					<select name="qualifying_type" id="qualifying_type">
						<option value="0"># of tickets</option>
					</select>
				</div>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'Amount'; ?>
				<?php $input['name'] = 'qualifying_type_amount'; ?>
				<?php include 'parts/atoms/input-text.php'; ?>
			</div>
		</div>
		<div class="row bar"></div>
		<div class="title_3">
			<h3>Apply Price Break To:</h3>
		</div>
		<div class="separator"></div>
		<div class="row">
			<?php $input['title'] = 'General Admission'; ?>
			<?php $input['name'] = 'apply_general_admission'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row">
			<?php $input['title'] = 'VIP'; ?>
			<?php $input['name'] = 'apply_vip'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row bar"></div>
		<div class="row">
			<?php $input['title'] = 'Price Breaks Across Ticket Types'; ?>
			<?php $input['name'] = 'price_break_across'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row bar"></div>
		<div class="title_3">
			<h3>Promo Code Relationship:</h3>
		</div>
		<div class="separator"></div>
		<div class="row">
			<?php $input['title'] = 'Allow Promo Codes with Price'; ?>
			<?php $input['name'] = 'allow_promo_codes_with_price'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row">
			<?php $input['title'] = 'Allow The Greater Discount Only'; ?>
			<?php $input['name'] = 'allow_the_greater_discount_only'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		<div class="row bar"></div>
		<div class="row">
			<?php $input['placeholder'] = 'Checkout Banner Text'; ?>
			<?php $input['name'] = 'checkout_banner_text'; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>
		<?php $link['text'] = 'Add Price Break'; ?>
		<?php $link['id'] = 'add_price_brek'; ?>
		<?php $link['class'] = 'title_1'; ?>
		<?php include 'parts/atoms/link-1.php'; ?>
	</div>
</div>