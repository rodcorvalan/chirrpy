<div class="section_header">
	<div class="title">
		<h2>Section Title</h2>
	</div>
	<nav class="screens">
		<ul>
			<li class="active"><a href="#" data-editor="0">Editor<br/>Preview</a></li>
			<li><a href="#" data-editor="1">Desktop<br/>Preview</a></li>
		</ul>
	</nav>
	<nav class="menus">
		<a href="#" id="menu_icon"><span class="fa fa-bars"></span></a>
		<ul>
			<li><a href="#">Help</a></li>
			<li><a href="#">My Account</a></li>
		</ul>
	</nav>
</div>