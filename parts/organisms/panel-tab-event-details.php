<div class="accordion">

	<a href="#" class="title title_1">Event Details<span class="fa fa-chevron-down"></span></a>
	
	<div class="content">

		<div class="row">
			<?php $input['placeholder'] = 'Event Title'; ?>
			<?php $input['name'] = 'event_title'; ?>
			<?php $input['required'] = true; ?>
			<?php $input['class'] = ''; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>

		<div class="row">
			<?php $input['placeholder'] = 'Venue Name'; ?>
			<?php $input['name'] = 'venue_name'; ?>
			<?php $input['required'] = true; ?>
			<?php $input['class'] = ''; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>

		<div class="row">
			<?php $input['placeholder'] = 'Venue Address'; ?>
			<?php $input['name'] = 'venue_address'; ?>
			<?php $input['required'] = true; ?>
			<?php $input['class'] = ''; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>

		<div class="row">
			<?php $input['placeholder'] = 'Venue Address Line 2'; ?>
			<?php $input['name'] = 'venue_address_line_2'; ?>
			<?php $input['required'] = false; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>

		<div class="row">
			<?php $input['placeholder'] = 'Zip Code'; ?>
			<?php $input['name'] = 'zip_code'; ?>
			<?php $input['required'] = true; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>

		<div class="separator"></div>

		<div class="title_2">
			<h3>Event Day</h3>
		</div>

		<div class="separator"></div>
		
		<div class="row">

			<div class="column_2">
				<?php $input['placeholder'] = 'Start Date'; ?>
				<?php $input['name'] = 'start_date'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'start_date'; ?>
				<?php include 'parts/atoms/input-date.php'; ?>
			</div>

			<div class="column_2 last">
				<?php $input['placeholder'] = 'End Date'; ?>
				<?php $input['name'] = 'end_date'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'end_date'; ?>
				<?php include 'parts/atoms/input-date.php'; ?>
			</div>

		</div>

		<div class="row">

			<div class="column_2">
				<?php $input['placeholder'] = 'Start Time'; ?>
				<?php $input['name'] = 'start_time'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'start_time'; ?>
				<?php include 'parts/atoms/input-time.php'; ?>
			</div>

			<div class="column_2 last">
				<?php $input['placeholder'] = 'End Time'; ?>
				<?php $input['name'] = 'end_time'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'end_time'; ?>
				<?php include 'parts/atoms/input-time.php'; ?>
			</div>

		</div>

		<div class="separator"></div>
		
		<div class="title_2">
			<h3>Event Day 2 <span class="fa fa-close"></span></h3>
		</div>

		<div class="separator"></div>

		<div class="row">

			<div class="column_2">
				<?php $input['placeholder'] = 'Start Date'; ?>
				<?php $input['name'] = 'start_date_2'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'start_date_2'; ?>
				<?php include 'parts/atoms/input-date.php'; ?>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'End Date'; ?>
				<?php $input['name'] = 'end_date_2'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'end_date_2'; ?>
				<?php include 'parts/atoms/input-date.php'; ?>
			</div>

		</div>
		<div class="row">

			<div class="column_2">
				<?php $input['placeholder'] = 'Start Time'; ?>
				<?php $input['name'] = 'start_time_2'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'start_time_2'; ?>
				<?php include 'parts/atoms/input-time.php'; ?>
			</div>

			<div class="column_2 last">
				<?php $input['placeholder'] = 'End Time'; ?>
				<?php $input['name'] = 'end_time_2'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'end_time_2'; ?>
				<?php include 'parts/atoms/input-time.php'; ?>
			</div>

		</div>

		<?php $link['text'] = 'Add Event Name'; ?>
		<?php $link['id'] = 'add_event_name'; ?>
		<?php $link['class'] = 'title_1'; ?>
		<?php include 'parts/atoms/link-1.php'; ?>


	</div>
</div>



<div class="accordion">

	<a href="#" class="title title_1">Main Event Photo<span class="fa fa-chevron-down"></span></a>
	
	<div class="content">
		
		<div class="row">
			<div class="upload upload_single"></div>
		</div>

		<div class="row">
			<div class="note">
				<p>RECOMMENDED SIZE: 1600px x 500px</p>
				<p>.PNG or .JPG only</p>
				<p>MAX SIZE: 10mb</p>
			</div>
		</div>

		<div class="row">
			<a href="#" class="link_1">
				Download Photoshop Template
			</a>
		</div>

		<div class="row bar"></div>
		
		<div class="row">
			<?php $input['title'] = 'Disable Main Event Image'; ?>
			<?php $input['name'] = 'disable_main_event_image'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>

		<div class="row">
			<?php $input['title'] = 'Disable Image Stretching'; ?>
			<?php $input['name'] = 'disable_image_stretching'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>
		
		<div class="row">
			<?php $input['title'] = 'Auto Darken'; ?>
			<?php $input['name'] = 'auto_darken'; ?>
			<?php $input['name_value'] = 'auto_darken_value'; ?>
			<?php include 'parts/atoms/input-range.php'; ?>
		</div>
		
		<div class="row">
			<?php $input['title'] = 'Disable Overlay Text'; ?>
			<?php $input['name'] = 'disable_overlay_text'; ?>
			<?php include 'parts/atoms/input-checkbox.php'; ?>
		</div>

	</div>

</div>

<div class="accordion">

	<a href="#" class="title title_1">Event Description<span class="fa fa-chevron-down"></span></a>
	
	<div class="content">

		<?php $textarea['name'] = 'event_description_textarea'; ?>
		<?php include 'parts/atoms/textarea-editor.php'; ?>
		
	</div>

</div>

<div class="accordion">

	<a href="#" class="title title_1">Content Block<span class="fa fa-chevron-down"></span></a>
	
	<div class="content">
		
		<div class="row">
			<div class="title_2">
				<h3>Content Block<span class="fa fa-close"></span></h3>
			</div>
		</div>

		<div class="row">
			<div class="column_2">
				<?php $input['placeholder'] = 'Title'; ?>
				<?php $input['name'] = 'content_title'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['class'] = ''; ?>
				<?php include 'parts/atoms/input-text.php'; ?>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'Position'; ?>
				<?php $input['name'] = 'position'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['class'] = ''; ?>
				<?php include 'parts/atoms/input-number.php'; ?>
			</div>
		</div>

		<div class="row">
			<div class="column_2">
				<div class="input_select">
					<label for="kind">Kind</label>
					<select name="kind" id="kind">
						<option value="text">Text</option>
						<option value="text">Map</option>
					</select>
				</div>
			</div>
			<div class="column_2 last">
				<div class="input_select">
					<label for="content_width">Width</label>
					<select name="content_width" id="content_width">
						<option value="text">Full Width</option>
						<option value="text">Other</option>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<?php $textarea['id'] = 'event_description_textarea'; ?>
			<?php include 'parts/atoms/textarea-editor.php'; ?>
		</div>

		<div class="row">
			<div class="map" id="map"></div>
			<input type="hidden" name="lat" id="lat" value="-32.9201485" />
			<input type="hidden" name="lng" id="lng" value="-68.8483057" />
		</div>

		<div class="row">
			<div class="upload upload_multiple"></div>
		</div>

		<div class="row">
			<div class="upload_queue">
				<h4>Queue</h4>
				<div class="list"></div>
			</div>
		</div>

		<div class="row">
			<div class="note">
				<p>RECOMMENDED SIZE: 1600px x 500px</p>
				<p>.PNG or .JPG only</p>
				<p>MAX SIZE: 10mb</p>
			</div>
		</div>

	</div>

</div>