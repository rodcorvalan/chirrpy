<div class="panel">

	<form>

		<div class="tabs">
			<ul>
				<li class="tab active"><a href="#" data-tab="1">Event Details</a></li>
				<li class="tab"><a href="#" data-tab="2">Tickets</a></li>
				<li class="tab"><a href="#" data-tab="3">Payment Gateways</a></li>
				<li class="tab"><a href="#" data-tab="4">Advanced</a></li>
			</ul>
		</div>

		<div class="tabs_content">
			<div class="tab_content tab_content_1 active">
				<?php include 'panel-tab-event-details.php'; ?>
			</div>
			<div class="tab_content tab_content_2">
				<?php include 'panel-tab-tickets.php'; ?>
			</div>
			<div class="tab_content tab_content_3">
				<?php include 'panel-tab-payment-gateways.php'; ?>
			</div>
			<div class="tab_content tab_content_4">
				<?php include 'panel-tab-advanced.php'; ?>
			</div>
		</div>

	</form>

</div>