<div class="accordion">

	<a href="#" class="title title_1">Ticket Types<span class="fa fa-chevron-down"></span></a>

	<div class="content">

		<div class="row">
			<?php $input['placeholder'] = 'Ticket Type'; ?>
			<?php $input['name'] = 'ticket_type'; ?>
			<?php $input['required'] = true; ?>
			<?php $input['class'] = ''; ?>
			<?php include 'parts/atoms/input-text.php'; ?>
		</div>

		<div class="row">
			<div class="column_2">
				<div class="column_text_checkbox">
					<?php $input['placeholder'] = 'Ticket Price'; ?>
					<?php $input['name'] = 'ticket_price'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					<div class="input_checkbox_label_up">
						<input type="checkbox" id="ticket_free" name="ticket_free" />
						<label for="ticket_free">Free</label>
					</div>
				</div>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'QTY AVAIL'; ?>
				<?php $input['name'] = 'quantity_available'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['class'] = ''; ?>
				<?php include 'parts/atoms/input-text.php'; ?>
			</div>
		</div>

		<div class="row">
			<div class="column_2">
				<?php $input['placeholder'] = 'Fees Pay By'; ?>
				<?php $input['name'] = 'fees_pay_by'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['class'] = ''; ?>
				<?php include 'parts/atoms/input-text.php'; ?>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'Show Qty Avail?'; ?>
				<?php $input['name'] = 'show_quantity_available'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['class'] = ''; ?>
				<?php include 'parts/atoms/input-number.php'; ?>
			</div>
		</div>


		<div class="row">
			<div class="column_2">
				<?php $input['placeholder'] = 'On Sale Start Date'; ?>
				<?php $input['name'] = 'on_sale_start_date'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'on_sale_start_date'; ?>
				<?php include 'parts/atoms/input-date.php'; ?>
			</div>
			<div class="column_2 last">
				<?php $input['placeholder'] = 'On Sale Start Time'; ?>
				<?php $input['name'] = 'on_sale_start_time'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'on_sale_start_time'; ?>
				<?php include 'parts/atoms/input-time.php'; ?>
			</div>
		</div>
		<div class="row">
			<div class="column_2">
				<?php $input['placeholder'] = 'On Sale End Date'; ?>
				<?php $input['name'] = 'on_sale_end_date'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'on_sale_end_date'; ?>
				<?php include 'parts/atoms/input-date.php'; ?>
			</div>

			<div class="column_2 last">
				<?php $input['placeholder'] = 'On Sale End Time'; ?>
				<?php $input['name'] = 'on_sale_end_time'; ?>
				<?php $input['required'] = true; ?>
				<?php $input['id'] = 'on_sale_end_time'; ?>
				<?php include 'parts/atoms/input-time.php'; ?>
			</div>
		</div>

		<div class="row">
			<textarea name="ticket_description" placeholder="Description"></textarea>
		</div>

		<div class="separator"></div>

		<?php $link['text'] = 'Add Ticket Type'; ?>
		<?php $link['id'] = 'add_ticket_type'; ?>
		<?php $link['class'] = 'title_1'; ?>
		<?php include 'parts/atoms/link-1.php'; ?>

	</div>

</div>

