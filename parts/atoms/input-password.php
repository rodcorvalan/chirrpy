<div class="input_password <?php if ($input['parent_class']) echo $input['parent_class']; ?>">
	<label for="<?php echo $input['name']; ?>"><?php echo $input['placeholder']; ?><?php if ($input['required']) : ?><span class="required">*</span><?php endif; ?></label>
	<input type="password" name="<?php echo $input['name']; ?>" id="<?php echo $input['name']; ?>" class="<?php echo $input['class']; ?>" <?php if ($input['required']) : ?>required<?php endif; ?> />
</div>
<?php $input = null; ?>