<div class="input_date">
	<label for="<?php echo $input['name']; ?>"><?php echo $input['placeholder']; ?><?php if ($input['required']) : ?><span class="required">*</span><?php endif; ?></label>
	<input type="text" id="<?php echo $input['id']; ?>" name="<?php echo $input['name']; ?>" <?php if ($input['required']) : ?>required<?php endif; ?> readonly />
</div>
