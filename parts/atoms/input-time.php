<div class="input_time">
	<label for="<?php echo $input['name']; ?>"><?php echo $input['placeholder']; ?><?php if ($input['required']) : ?><span class="required">*</span><?php endif; ?></label>
	<input type="text" name="<?php echo $input['name']; ?>" <?php if ($input['required']) : ?>required<?php endif; ?> readonly />
</div>
