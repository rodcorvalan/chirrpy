<div class="input_number">
	<label for="<?php echo $input['name']; ?>"><?php echo $input['placeholder']; ?><?php if ($input['required']) : ?><span class="required">*</span><?php endif; ?></label>
	<input type="number" name="<?php echo $input['name']; ?>" class="<?php echo $input['class']; ?>"  <?php if ($input['required']) : ?>required<?php endif; ?> />
</div>
