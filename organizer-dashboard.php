<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Chirrpy</title>
	<link rel="stylesheet" href="stylesheets/style.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>


<div class="header">
	<div class="logo">
		<h1><a href="#"><img src="images/logo.png" /></a></h1>
	</div>
	<div class="main_menu_icon">
		<a href="#" id="menu_icon"><span class="fa fa-bars"></span></a>
	</div>
	<nav class="main_menu">
		<ul>
			<li><a href="#"><span>Help</span></a></li>
			<li><a href="#"><span>Organizer <br/>Dashboard</span></a></li>
			<li><a href="#"><span>My Account</span></a></li>
		</ul>
		<ul class="hide_from_l">
			<li><a href="#"><span>Home</span></a></li>
			<li><a href="#"><span>Messages</span></a></li>
			<li><a href="#"><span>Events</span></a></li>
			<li><a href="#"><span>Attendes</span></a></li>
			<li><a href="#"><span>Reports</span></a></li>
			<li><a href="#"><span>Promo Codes</span></a></li>
			<li class="active"><a href="#"><span>Financial Settings</span></a></li>
			<li><a href="#"><span>Advanced</span></a></li>
			<li><a href="#"><span>Integrations</span></a></li>
			<li><a href="#"><span>User</span></a></li>
			<li><a href="#"><span>Organizer Account</span></a></li>
			<li><a href="#"><span>Enterprise</span></a></li>
		</ul>
	</nav>
</div>

<div class="organizer_dashboard">

	<div class="organizer_dashboard_left">
		<nav class="main_menu">
			<ul>
				<li><a href="#"><span>Home</span></a></li>
				<li><a href="#"><span>Messages</span></a></li>
				<li><a href="#"><span>Events</span></a></li>
				<li><a href="#"><span>Attendes</span></a></li>
				<li><a href="#"><span>Reports</span></a></li>
				<li><a href="#"><span>Promo Codes</span></a></li>
				<li class="active"><a href="#"><span>Financial Settings</span></a></li>
				<li><a href="#"><span>Advanced</span></a></li>
				<li><a href="#"><span>Integrations</span></a></li>
				<li><a href="#"><span>User</span></a></li>
				<li><a href="#"><span>Organizer Account</span></a></li>
				<li><a href="#"><span>Enterprise</span></a></li>
			</ul>
		</nav>
		<nav class="bottom_menu">
			<ul>
				<li><a href="#"><span>Organizer Support</span></a></li>
				<li><a href="#"><span>Live Chat</span></a></li>
			</ul>
		</nav>
	</div>

	<div class="organizer_dashboard_center">


		<div class="organizer_dashboard_section financial">
			
			<div class="organizer_dashboard_section_title">
				
				<h2>Financial</h2>
				
				<div class="box box_payments_gateways">

					<div class="title_box">Payment Gateways</div>

					<div class="accept_cards">
						<div class="title_line"><h4>Accept Cards</h4></div>
						<div class="accept_cards_wrapper">
							<div class="payments_box anovia_box">
								<div class="image"><img src="images/anovia.png"></div>
								<div class="fees">3.99% + $.99</div>
								<div class="status"><span class="green">CONNECTED</span></div>
							</div>
							<div class="or">OR</div>
							<div class="payments_box expect_box">
								<div class="image"><img src="images/expect-payments.png"></div>
								<div class="fees">3.99% + $.99</div>
								<div class="status">NOT CONNECTED</div>
							</div>
						</div>
					</div>

					<div class="accept_paypal">
						<div class="title_line"><h4>Accept Paypal</h4></div>
						<div class="payments_box paypal_box">
							<div class="image"><img src="images/paypal.png"></div>
							<div class="fees">PAYPAL FEES + 1% + $.99</div>
							<div class="status">NOT CONNECTED</div>
						</div>
					</div>

				</div>

				<div class="box">
					<div class="title_box">Payout History</div>
					<table>
						<thead>
							<tr>
								<th>Payout ID #</th>
								<th>Event Name</th>
								<th>Processing Date</th>
								<th>Payout Gateway</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<?php for ($i=0; $i < 10; $i++) : ?>
							<tr>
								<td><a href="#"><?php echo $i; ?></a></td>
								<td><a href="#">Wine and Food Festival</a></td>
								<td>02-05-16</td>
								<td>ANOVIA</td>
								<td>$25,246.34</td>
							</tr>
							<?php endfor; ?>
						</tbody>
					</table>
				</div>

			</div>

			

		</div>
		


		<div class="organizer_dashboard_section financial_settings">
			<div class="organizer_dashboard_section_title">
				<h2>Financial Settings</h2>
			</div>
			<div class="box box_financial_settings">
				<div class="logo">
					<img src="images/anovia.png" />
				</div>
				<div class="content">
					<div class="row_1">
						<div class="subtitle"><h3>Processing Fee Structure</h3></div>
						<p>Anovia Payments charges 3.99% on the sum of the (order amount plus $.99 per ticket)</p>
						<p>Example: Processing fees for two $50 ticket</p>
						<p>(($50 X 2)+($.99 X 2)) X 3.99% = $4.07</p>
					</div>
					<div class="row_2">
						<p>Home Page: <a href="#">anoviapayments.com</a></p>
						<p>Status: Not Connected <span class="green">CONNECTED</span></p>
					</div>
					<div class="row_3">
						<a href="#" class="button button_1">CONNECT</a>
						<a href="#" class="button button_3">DISCONNECT</a>
					</div>
				</div>
			</div>
			<div class="title_box">Anovia Payment Setup</div>
			<div class="box box_anovia_payment_setup">
				<div class="title_line"><h4>STEP 1 : Personal Info</h4></div>
				<div class="column_1">
					<div class="row">
					<?php $input['placeholder'] = 'First Name'; ?>
					<?php $input['name'] = 'anovia_first_name'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Last Name'; ?>
					<?php $input['name'] = 'anovia_last_name'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Email'; ?>
					<?php $input['name'] = 'anovia_email'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-email.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Phone'; ?>
					<?php $input['name'] = 'anovia_phone'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Date of Birth'; ?>
					<?php $input['name'] = 'anovia_birth'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-date.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Social Security #'; ?>
					<?php $input['name'] = 'anovia_social_security'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
				</div>
				<div class="column_2">
					<div class="row">
					<?php $input['placeholder'] = 'Home Address'; ?>
					<?php $input['name'] = 'anovia_home_address'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Home Address Line 2'; ?>
					<?php $input['name'] = 'anovia_home_address_line_2'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'City'; ?>
					<?php $input['name'] = 'anovia_home_city'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
						<div class="input_select">
							<label for="anovia_state">State<span class="required">*</span></label>
							<select name="anovia_state" id="anovia_state">
								<option value="text">State 1</option>
								<option value="text">State 2</option>
							</select>
						</div>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'ZIPCODE'; ?>
					<?php $input['name'] = 'anovia_zipcode'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
				</div>
				<div class="title_line"><h4>STEP 2 : Account Info</h4></div>
				<div class="column_only input_half">
					<div class="row">
						<div class="input_select">
							<label for="anovia_account_type">Account Type<span class="required">*</span></label>
							<select name="anovia_account_type" id="anovia_account_type">
								<option value="text">Bussiness</option>
								<option value="text">Other</option>
							</select>
						</div>
					</div>
					<div class="row">
					<?php $input['placeholder'] = "Bussiness or event name to appear on attendees' credit card statements (16 MAX)"; ?>
					<?php $input['name'] = 'anovia_credit_card'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
				</div>
				<div class="title_line"><h4>STEP 3 : Deposit Info</h4></div>
				<div class="column_1">
					<div class="row">
					<?php $input['placeholder'] = 'Bank Account Holder Name'; ?>
					<?php $input['name'] = 'anovia_bank_account_holder_name'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
						<div class="input_select">
							<label for="anovia_bank_account_type">Bank Account Type<span class="required">*</span></label>
							<select name="anovia_bank_account_type" id="anovia_bank_account_type">
								<option value="text">Checking</option>
								<option value="text">Other</option>
							</select>
						</div>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Email'; ?>
					<?php $input['name'] = 'anovia_bank_account_email'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-email.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Phone'; ?>
					<?php $input['name'] = 'anovia_bank_account_phone'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
				</div>
				<div class="column_2">
					<div class="row">
					<?php $input['placeholder'] = 'Routing Number'; ?>
					<?php $input['name'] = 'anovia_bank_account_routing_number'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Retype Routing Number'; ?>
					<?php $input['name'] = 'anovia_bank_account_retype_routing_number'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Account Number'; ?>
					<?php $input['name'] = 'anovia_bank_account_number'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
					<div class="row">
					<?php $input['placeholder'] = 'Retype Account Number'; ?>
					<?php $input['name'] = 'anovia_bank_account_retype_number'; ?>
					<?php $input['required'] = true; ?>
					<?php $input['class'] = ''; ?>
					<?php include 'parts/atoms/input-text.php'; ?>
					</div>
				</div>
				<div class="column_only">
					<div class="row">
						<img src="images/deposit-info.png" />
					</div>
				</div>
				<div class="column_only last">
					<div class="row">
						<a href="#" class="button button_1">APPLY</a>
						<div class="quote">
							<p>By clicking apply you are agreeing to the Annovia payments terms and conditions in addition to the expect.events terms and conditions: <a href="#">anovia.com/termsandconditions</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	

	</div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAOCeCFFOptXCFvfZIOp6SMAgBIKffGCvc"></script>
<script src="javascripts/wickedpicker.min.js"></script>
<script src="javascripts/tinymce/tinymce.min.js"></script>
<script src="javascripts/select2.full.min.js"></script>
<script src="javascripts/formstone.core.js"></script>
<script src="javascripts/formstone.upload.js"></script>
<script src="javascripts/armix.menu.js"></script>	
<script src="javascripts/armix.screen.js"></script>	
<script src="javascripts/armix.tabs.js"></script>	
<script src="javascripts/armix.inputs.js"></script>	
<script src="javascripts/armix.accordion.js"></script>	
<script src="javascripts/armix.lightbox.js"></script>	
<script src="javascripts/armix.preview.js"></script>	

</body>
</html>