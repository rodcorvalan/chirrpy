<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Chirrpy</title>
	<link rel="stylesheet" href="stylesheets/style.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>

<?php include 'parts/organisms/preview.php'; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOCeCFFOptXCFvfZIOp6SMAgBIKffGCvc"></script>
<script src="javascripts/armix.lightbox.js"></script>	
<script src="javascripts/armix.preview.js"></script>	

</body>
</html>

