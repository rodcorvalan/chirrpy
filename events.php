<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Chirrpy</title>
	<link rel="stylesheet" href="stylesheets/style.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>


<div class="header">
	<div class="logo">
		<h1><a href="#"><img src="images/logo.png" /></a></h1>
	</div>
	<div class="main_menu_icon">
		<a href="#" id="menu_icon"><span class="fa fa-bars"></span></a>
	</div>
	<nav class="main_menu">
		<ul>
			<li><a href="#"><span>Help</span></a></li>
			<li><a href="#"><span>Organizer <br/>Dashboard</span></a></li>
			<li><a href="#"><span>My Account</span></a></li>
		</ul>
		<ul class="hide_from_l">
			<li><a href="#"><span>Home</span></a></li>
			<li><a href="#"><span>Messages</span></a></li>
			<li class="active"><a href="#"><span>Events</span></a></li>
			<li><a href="#"><span>Attendes</span></a></li>
			<li><a href="#"><span>Reports</span></a></li>
			<li><a href="#"><span>Promo Codes</span></a></li>
			<li><a href="#"><span>Financial Settings</span></a></li>
			<li><a href="#"><span>Advanced</span></a></li>
			<li><a href="#"><span>Integrations</span></a></li>
			<li><a href="#"><span>User</span></a></li>
			<li><a href="#"><span>Organizer Account</span></a></li>
			<li><a href="#"><span>Enterprise</span></a></li>
		</ul>
	</nav>
</div>

<div class="organizer_dashboard">

	<div class="organizer_dashboard_left">
		<nav class="main_menu">
			<ul>
				<li><a href="#"><span>Home</span></a></li>
				<li><a href="#"><span>Messages</span></a></li>
				<li class="active"><a href="#"><span>Events</span></a></li>
				<li><a href="#"><span>Attendes</span></a></li>
				<li><a href="#"><span>Reports</span></a></li>
				<li><a href="#"><span>Promo Codes</span></a></li>
				<li><a href="#"><span>Financial Settings</span></a></li>
				<li><a href="#"><span>Advanced</span></a></li>
				<li><a href="#"><span>Integrations</span></a></li>
				<li><a href="#"><span>User</span></a></li>
				<li><a href="#"><span>Organizer Account</span></a></li>
				<li><a href="#"><span>Enterprise</span></a></li>
			</ul>
		</nav>
		<nav class="bottom_menu">
			<ul>
				<li><a href="#"><span>Organizer Support</span></a></li>
				<li><a href="#"><span>Live Chat</span></a></li>
			</ul>
		</nav>
	</div>

	<div class="organizer_dashboard_center">

		<div class="organizer_dashboard_section organizer_dashboard_events">

			<div class="organizer_dashboard_section_title">
				<h2>Events</h2>
			</div>

			<div class="box">

				<div class="title_box">Wine and Food Fetival</div>

				<div class="four_buttons">
					<div class="four_buttons_wrapper">
						<button class="button button_4">Edit</button>
						<button class="button button_4">Attendees and Orders</button>
						<button class="button button_4">Chech In</button>
						<button class="button button_4">View Payout</button>
					</div>
				</div>

				<div class="table_wrapper">
					<table>
						<thead>
							<tr>
								<th>Event Name</th>
								<th>Date</th>
								<th>Ticket Sold</th>
								<th>Total Sales</th>
								<th>Select Status</th>
								<th>View</th>
								<th>Chech In</th>
								<th>Edit</th>
								<th>Duplicate</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Wine and Food Festival</td>
								<td>01/01/06</td>
								<td>142 of 500</td>
								<td class="blue">$3.000,00</td>
								<td class="green">Publish</td>
								<td><a href="#">View</a></td>
								<td><a href="#">Check In</a></td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Duplicate</a></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="table_wrapper">
					<table>
						<thead>
							<tr>
								<th>Ticket Type</th>
								<th>Date</th>
								<th>Ticket Sold</th>
								<th>Price</th>
								<th>Revenue</th>
								<th>Discounts</th>
								<th>Fees</th>
								<th>Taxes</th>
								<th>Total Sales</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>General Admission</td>
								<td>01/01/01</td>
								<td>100 of 400</td>
								<td>$20.00</td>
								<td>$3.000,00</td>
								<td>$0,00</td>
								<td>$0,00</td>
								<td>$0,00</td>
								<td>$3.000,00</td>
							</tr>
							<tr>
								<td>VIP</td>
								<td>01/01/01</td>
								<td>100 of 400</td>
								<td>$20.00</td>
								<td>$3.000,00</td>
								<td>$0,00</td>
								<td>$0,00</td>
								<td>$0,00</td>
								<td>$3.000,00</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="button_delete">
					<button class="button button_4">Delete</button>
				</div>

			</div>

		</div>

		<div class="organizer_dashboard_section organizer_dashboard_events">

			<div class="organizer_dashboard_section_title">
				<h2>Events</h2>
			</div>

			<div class="button_create">
				<button class="button button_1">CREATE EVENT</button>
			</div>

			<div class="box">

				<div class="title_box">Manage Events</div>

				<div class="search">
					<div class="input_text">
						<input type="text" name="" placeholder="ENTER EVENT NAME" required />
					</div>
					<div class="search_buttons">
						<button class="button button_2">Clear</button>
						<button class="button button_2">Search</button>
					</div>
				</div>

				<div class="table_wrapper">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>Date</th>
								<th>Ticket Sold</th>
								<th>Total Sales</th>
								<th>Select Status</th>
								<th>View</th>
								<th>Chech In</th>
								<th>Edit</th>
								<th>Duplicate</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Wine and Food Festival</td>
								<td>01/01/06</td>
								<td>142 of 500</td>
								<td>$3.000,00</td>
								<td class="green">Publish</td>
								<td><a href="#">View</a></td>
								<td><a href="#">Check In</a></td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Duplicate</a></td>
							</tr>
							<tr>
								<td>Wine and Food Festival</td>
								<td>01/01/06</td>
								<td>142 of 500</td>
								<td>$3.000,00</td>
								<td class="blue">Unpublish</td>
								<td><a href="#">View</a></td>
								<td><a href="#">Check In</a></td>
								<td><a href="#">Edit</a></td>
								<td><a href="#">Duplicate</a></td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>

		</div>

	

	</div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAOCeCFFOptXCFvfZIOp6SMAgBIKffGCvc"></script>
<script src="javascripts/wickedpicker.min.js"></script>
<script src="javascripts/tinymce/tinymce.min.js"></script>
<script src="javascripts/select2.full.min.js"></script>
<script src="javascripts/formstone.core.js"></script>
<script src="javascripts/formstone.upload.js"></script>
<script src="javascripts/armix.menu.js"></script>	
<script src="javascripts/armix.screen.js"></script>	
<script src="javascripts/armix.tabs.js"></script>	
<script src="javascripts/armix.inputs.js"></script>	
<script src="javascripts/armix.accordion.js"></script>	
<script src="javascripts/armix.lightbox.js"></script>	
<script src="javascripts/armix.preview.js"></script>	

</body>
</html>