<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Chirrpy</title>
	<link rel="stylesheet" href="stylesheets/style.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
</head>
<body>


<div class="header">
	<div class="logo">
		<h1><a href="#"><img src="images/logo.png" /></a></h1>
	</div>
	<nav class="screens">
		<ul>
			<li class="active"><a href="#" data-editor="0">Editor<br/>Preview</a></li>
			<li><a href="#" data-editor="1">Desktop<br/> Preview</a></li>
		</ul>
	</nav>
	<div class="main_menu_icon">
		<a href="#" id="menu_icon"><span class="fa fa-bars"></span></a>
	</div>
	<nav class="main_menu">
		<ul>
			<li><a href="#"><span>Help</span></a></li>
			<li><a href="#"><span>My Account</span></a></li>
		</ul>
	</nav>
</div>


<?php include 'parts/organisms/section-main.php'; ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyAOCeCFFOptXCFvfZIOp6SMAgBIKffGCvc"></script>
<script src="javascripts/wickedpicker.min.js"></script>
<script src="javascripts/tinymce/tinymce.min.js"></script>
<script src="javascripts/select2.full.min.js"></script>
<script src="javascripts/formstone.core.js"></script>
<script src="javascripts/formstone.upload.js"></script>
<script src="javascripts/armix.menu.js"></script>	
<script src="javascripts/armix.screen.js"></script>	
<script src="javascripts/armix.tabs.js"></script>	
<script src="javascripts/armix.inputs.js"></script>	
<script src="javascripts/armix.accordion.js"></script>	
<script src="javascripts/armix.lightbox.js"></script>	
<script src="javascripts/armix.preview.js"></script>	

</body>
</html>