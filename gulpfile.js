'use strict';

// Require
var gulp 		= require('gulp');
var sass 		= require('gulp-sass');
var sourcemaps 	= require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var pleeease    = require('gulp-pleeease');

// Proxy URL
var URL   = 'chirrpy.dev';
var theme = 'chirrpy';

// Folders
var SRC  = './sass/**/*.scss';
var DEST = './stylesheets/';
var CSS  = './stylesheets/style.css';
var FILE = './sass/style.scss';

//
// Mini
//

gulp.task('miniwatch', () => {
	gulp.watch(SRC, ['minisass']);
});

gulp.task('minisass',() => {
	gulp.src(FILE)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(DEST))
	;
});

// 
// Dev/Normal
// 

gulp.task('watch', function () {
	gulp.watch(SRC, ['sass']);
});

gulp.task('sass',function(){
	gulp.src(FILE)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(DEST))
		.pipe(browserSync.stream())
	;
});

gulp.task('browsersync',function(){
	browserSync.init({proxy: URL,files: ['stylesheets/style.css','*.php','parts/**/*.php','*.html','parts/**/*.html']});
});

gulp.task('prod',function(){
	gulp.src(FILE)
		.pipe(sass())
		.pipe(pleeease())
		.pipe(gulp.dest(DEST))
	;
});


//
// Icons
//

var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');

var icon_font_name = 'icons';
var icon_source = 'images/icons/*.svg';
var icon_sass_file = '../../sass/config/_icons.scss';
var icon_taget_path = '../fonts/icons/';
var icon_font_dest = 'fonts/icons/';
var icon_template = 'sass/config/_icon-template.scss';

gulp.task('iconfont', function(){
  gulp.src(icon_source)
    .pipe(iconfontCss({
      fontName: icon_font_name,
      path : icon_template,
      targetPath: icon_sass_file,
      fontPath: icon_taget_path,
    }))
    .pipe(iconfont({
      fontName: icon_font_name,
      //normalize: true,
      //fontHeight: 1001
     }))
    .pipe(gulp.dest(icon_font_dest));
});